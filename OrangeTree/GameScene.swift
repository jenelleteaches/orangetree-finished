//
//  GameScene.swift
//  OrangeTree
//
//  Created by Parrot on 2019-02-19.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {

    var tree:SKSpriteNode!
    var orange:Orange!
    
    // gesture variables
    var startingTouchPosition: CGPoint = CGPoint(x:0, y:0)
    
    // draw a line
    var shapeNode = SKShapeNode()
    
    override func didMove(to view: SKView) {
        tree = self.childNode(withName: "tree") as! SKSpriteNode
        
        // setup a boundary aruound the scene
        self.physicsBody = SKPhysicsBody(edgeLoopFrom: self.frame)
        
        // setup the delegate
        self.physicsWorld.contactDelegate = self
        
        
        // configure shape node
        self.shapeNode.lineWidth = 20
        self.shapeNode.lineCap = .round
        self.shapeNode.strokeColor = UIColor(red: 0.5294, green: 0, blue: 0.5765, alpha: 0.8)
        addChild(shapeNode)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if (touch == nil) {
            return
        }
        
        let mousePosition = touch?.location(in: self)
        
        let spriteTouched = self.atPoint(mousePosition!)
        
        if (spriteTouched.name == "tree") {
            // shoot an orange
            print("i pressed the tree")
            
            // spawn an orange
            orange = Orange()
            
            orange.position = mousePosition!
            addChild(orange)
            
            orange.physicsBody?.isDynamic = false
            // get the starting position of mouse
            startingTouchPosition = mousePosition!
            
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        // person raised their finger
        let touch = touches.first
        if (touch == nil) {
            return
        }
        
        let endingPosition = touch!.location(in: self)
        
        let diffX = endingPosition.x - self.startingTouchPosition.x
        let diffY = endingPosition.y - self.startingTouchPosition.y

        let direction = CGVector(dx:diffX, dy:diffY)
        self.orange.physicsBody?.isDynamic = true
        self.orange.physicsBody?.applyImpulse(direction)
        
        // remove the line
        self.shapeNode.path = nil

    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if (touch == nil) {
            return
        }
        
        let mousePosition = touch!.location(in: self)
        
        orange.position = mousePosition
        
        //draw a line to show direction of orange
        let path = UIBezierPath()
        path.move(to: startingTouchPosition)
        path.addLine(to: mousePosition)
        self.shapeNode.path = path.cgPath
        
        
    }
    
    // notify us when two objects collide
    func didBegin(_ contact: SKPhysicsContact) {
        
        let nodeA = contact.bodyA.node
        let nodeB = contact.bodyB.node
        
        // Check that the bodies collided hard enough
        if contact.collisionImpulse > 15 {
            print("Collision impact: \(contact.collisionImpulse)")
            if nodeA?.name == "skull" {
                self.removeSkull(node: nodeA!)
                self.gameOver()
            } else if nodeB?.name == "skull" {
                self.removeSkull(node: nodeB!)
                self.gameOver()
            }
        }
    }
    
    // Function used to remove the Skull node from the scene
    func removeSkull(node: SKNode) {
        let reduceImageSize = SKAction.scale(to: 0.8, duration: 0.1)
        let removeNode = SKAction.removeFromParent()
        
        // group actions in a sequence
        let sequence = SKAction.sequence([reduceImageSize, removeNode])
        
        // run the sequence on the skull
        node.run(sequence)
        
    }
    
    // MARK: Restart the game
    func gameOver() {
        let message = SKLabelNode(text:"Game over. Restarting in 3 seconds!")
        message.position = CGPoint(x:self.size.width/2, y:self.size.height/2)
        message.fontColor = UIColor.magenta
        message.fontSize = 30
        message.fontName = "AvenirNext-Bold"
        
        addChild(message)
        
        // restart the game after 3 seconds
        perform(#selector(GameScene.restartGame), with: nil,
                afterDelay: 3)
    }
    
    @objc func restartGame() {
        let scene = GameScene(fileNamed:"GameScene")
        scene!.scaleMode = scaleMode
        view!.presentScene(scene)
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        
    }
}
